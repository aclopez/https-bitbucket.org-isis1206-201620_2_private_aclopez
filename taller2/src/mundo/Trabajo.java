package mundo;

import java.util.Date;

import mundo.Ocio.TipoEventoOcio;

public class Trabajo extends Evento
{

	public enum  TipoEventoTrabajo {
		JUNTAS,
		ALMUERZOS,
		CENAS,
		COCTELES,
		 PRECENTACIONES,
		INFORMES,
	}
	protected TipoEventoTrabajo tipo;
	
	public Trabajo(Date pFecha, String pLugar, TipoEventoTrabajo pTipo, boolean pObligatorio,boolean pFormal)
	{
		super(pFecha, pLugar, pObligatorio, pFormal);
		tipo=pTipo;
		// TODO Auto-generated constructor stub
	}

}
