package mundo;

import java.util.Date;

import mundo.Trabajo.TipoEventoTrabajo;

public class Socios extends Trabajo
{
	private Date fecha;
	private String lugar;
	private TipoEventoTrabajo tipo;
	private boolean obligatorio;
	private boolean formal;
	private String empresa;
	
	public Socios(String pEmpresa,Date pFecha, String pLugar, TipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal) 
	{
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		fecha= pFecha;
		lugar= pLugar;
		tipo=pTipo;
		obligatorio=pObligatorio;
		formal= pFormal;
		empresa=pEmpresa;
		
		// TODO Auto-generated constructor stub
	}
	public String darNegocio()
	{
		return empresa;
	}
	public Date darFecha()
	{
		return fecha;
	}
	public String darLugar()
	{
		return lugar;
	}
	public TipoEventoTrabajo darTipo()
	{
		return tipo;
	}
	public boolean darObligacion()
	{
		return obligatorio;
	}
	public boolean darFormalidad()
	{
		return formal;
	}
	public String DarEmpresa()
	{
		return empresa;
	}
}
