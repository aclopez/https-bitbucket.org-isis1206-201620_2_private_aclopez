package mundo;

import java.util.Date;

public class Clientes extends Trabajo
{
	private Date fecha;
	private String lugar;
	private TipoEventoTrabajo tipo;
	private boolean obligatorio;
	private boolean formal;
	private String negocio;
	
	public Clientes(String pnegocio,Date pFecha, String pLugar, TipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal) 
	{
		super(pFecha, pLugar, pTipo, pObligatorio, pFormal);
		fecha= pFecha;
		lugar= pLugar;
		tipo=pTipo;
		obligatorio=pObligatorio;
		formal=pFormal;
		negocio=pnegocio;
		
		// TODO Auto-generated constructor stub
	}
	public String darNegocio()
	{
		return negocio;
	}
	public Date darFecha()
	{
		return fecha;
	}
	public String darLugar()
	{
		return lugar;
	}
	public TipoEventoTrabajo darTipo()
	{
		return tipo;
	}
	public boolean darObligacion()
	{
		return obligatorio;
	}
	public boolean darFormalidad()
	{
		return formal;
	}
	public String DarNegocio()
	{
		return negocio;
	}
	public String toString()
	{
		String ob=" No es obligatorio";
		if(obligatorio==true)
		{
			ob="Si es obligatorio";
		}
		String mal="No";
		if(formal==true)
		{
			mal="Si";
		}
		
		return ""+fecha+" "+lugar+" "+tipo+" "+ob+" "+mal+" "+"es formal"+" "+negocio;
		
	}
}
